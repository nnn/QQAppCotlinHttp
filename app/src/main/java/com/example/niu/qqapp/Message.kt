package com.example.niu.qqapp

data class Message(
    var contactName: String,//发出人的名字
    var time: Long,//发出消息的时间
    var content: String//消息的内容
)