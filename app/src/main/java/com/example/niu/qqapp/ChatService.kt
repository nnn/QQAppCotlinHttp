package com.example.niu.qqapp

import com.example.niu.qqapp.adapter.ContactsPageListAdapter
import okhttp3.MultipartBody
import io.reactivex.Observable
import retrofit2.http.*
import retrofit2.http.GET

interface ChatService {
    @Multipart
    @POST("/apis/register")
    fun requestRegister(
        @Part fileData: MultipartBody.Part,
        @Query("name") name: String,
        @Query("password") password: String
    ): Observable<ServerResult<ContactsPageListAdapter.ContactInfo>>

    @GET("/apis/login")
    fun requestLogin(
        @Query("name") name: String,
        @Query("password") password: String?
    ): Observable<ServerResult<ContactsPageListAdapter.ContactInfo>>

    @GET("/apis/get_contacts")
    fun getContacts(): Observable<ServerResult<List<ContactsPageListAdapter.ContactInfo>>>

    @POST("/apis/upload_message")
    fun uploadMessage(@Body msg: Message): Observable<ServerResult<Any?>>

    @GET("/apis/get_messages")
    fun getMessagesFromIndex(@Query("after") index: Int): Observable<ServerResult<List<Message>>>
}