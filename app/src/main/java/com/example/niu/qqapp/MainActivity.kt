package com.example.niu.qqapp

import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.example.niu.qqapp.adapter.ContactsPageListAdapter
import com.example.niu.qqapp.databinding.ActivityMainBinding
import io.reactivex.disposables.Disposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : FragmentListener,AppCompatActivity() {
    //新式view binding，整天TM改来改去！
    private lateinit var binding: ActivityMainBinding

    //因为在onCreate()中就初始化，之后才被使用，
    //所以可以作为“延迟初始化”变量，就省了“?”了。
    private lateinit var retrofit: Retrofit
    private lateinit var chatService :ChatService
    //用于取消订阅
    private var disposable: Disposable? = null
    //用于显示进度条
    private var popupDialog: PopupWindow? = null

    //定义伴随对象，相当于Java中的static
    companion object {
        //保存我自己的帐户信息
        var myInfo: ContactsPageListAdapter.ContactInfo? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //创建Retrofit对象
        retrofit = Retrofit.Builder()
            //.baseUrl("http://10.0.2.2:8080")//在虚拟机中运行
            .baseUrl("http://10.0.2.2:8080")//在我的手机中运行
            //本来接口方法返回的是Call，由于现在返回类型变成了Observable，
            //所以必须设置Call适配器将Observable与Call结合起来
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            //Json数据自动转换
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //创建ChatService实例
        chatService = retrofit.create(ChatService::class.java)

        //将LoginFragment加入，作为首页
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = LoginFragment()
        fragmentTransaction.add(R.id.fragment_container, fragment)
        fragmentTransaction.commit()
    }

    override fun getChatServcie(): ChatService {
        return chatService
    }

    //显示进度条
    override fun showProgressBar() {
        //显示一个PopWindow，在这个Window中显示进度条
        //进度条
        val progressBar = ProgressBar(this)
        //设置进度条窗口覆盖整个父控件的范围，这样可以防止用户多次
        //点击按钮
        popupDialog = PopupWindow(
            progressBar,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        //将当前主窗口变成40%半透明，以实现背景变暗效果
        window.attributes.alpha = 0.4f
        //显示进度条窗口，将它放在Fragment所在的容器中，就可以覆盖Fragment的内容
        popupDialog?.showAtLocation(binding.fragmentContainer, Gravity.CENTER, 0, 0)
    }

    //隐藏进度条
    override fun hideProgressBar() {
        popupDialog?.dismiss()
        window.attributes.alpha = 1f
    }
}
