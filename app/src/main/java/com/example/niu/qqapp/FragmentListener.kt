package com.example.niu.qqapp

interface FragmentListener {
    fun getChatServcie() : ChatService
    fun hideProgressBar()
    fun showProgressBar()
}