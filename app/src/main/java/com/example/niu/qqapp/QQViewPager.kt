package com.example.niu.qqapp

import android.content.Context
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager
import android.view.MotionEvent


//必须实现带一个参数的构造方法
class QQViewPager : ViewPager {
    constructor(ctx: Context) : super(ctx) {

    }

    //必须实现此构造方法，否则在界面设计器中不能正常显示
    constructor(context: Context, attrs: AttributeSet) :
            super(context, attrs) {

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return false
    }
}