package com.example.niu.qqapp

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.example.niu.qqapp.adapter.ContactsPageListAdapter
import com.example.niu.qqapp.databinding.ActivityRegisterBinding
import com.example.niu.qqapp.databinding.ContentRegisterBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : AppCompatActivity() {
    //定义伴随对象，相当于Java中的static
    companion object {
        //启动拍照Activity的请求码
        val TAKE_PHOTO: Int = 1111
        //启动图库选择照片Activity的请求码
        val SELECT_PHOTO: Int = 1114
        //启动照片编辑Activity的请求码
        val CROP_PHOTO: Int = 1113
        //申请权限时的请求码
        val ASK_PERMISSIONS: Int = 1112
    }

    //新式view binding，整天TM改来改去！
    private lateinit var activityBinding: ActivityRegisterBinding
    // private lateinit var contentBiding: ContentRegisterBinding

    private var disposable: Disposable? = null

    //用于显示Bottom Sheet
    private var sheetDialog: BottomSheetDialog? = null

    //拍照所得图像保存到的绝对路径
    private var imageUri: Uri? = null

    private var retrofit: Retrofit? = null
    private var chatService :ChatService? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityBinding = ActivityRegisterBinding.inflate(layoutInflater)
        // 被include的资源，必须bind到容器资源的rootView！！！！
        // 但是，这样不行！为include添加id吧，通过activityBinding去引用
        // id就叫content
        // contentBiding = ContentRegisterBinding.bind(activityBinding.root)

        setContentView(activityBinding.root)

        //创建Retrofit对象
        retrofit = Retrofit.Builder()
            //.baseUrl("http://10.0.2.2:8080")//在虚拟机中运行
            .baseUrl("http://10.0.2.2:8080")//在我的手机中运行
            //本来接口方法返回的是Call，由于现在返回类型变成了Observable，
            //所以必须设置Call适配器将Observable与Call结合起来
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            //Json数据自动转换
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //创建Service代理对象
        chatService = retrofit?.create(ChatService::class.java)

        //用于显示Sheet
        this.sheetDialog = BottomSheetDialog(this)

        activityBinding.content.imageViewAvatar.setOnClickListener{
            val view = layoutInflater.inflate(R.layout.image_pick_sheet_menu, null)
            //获取sheet中的项，设置它们的Click侦听器
            view.findViewById<TextView>(R.id.sheetItemTakePhoto).setOnClickListener{
                //拍照

                // 保存需要申请的权限
                val permissionsList = ArrayList<String>()
                //检查是否有使用相机的权限
                if (ActivityCompat.checkSelfPermission(
                        this@RegisterActivity,
                        Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted
                    permissionsList.add(Manifest.permission.CAMERA)
                }
                //检查是否有使用外部存储的权限
                if (ActivityCompat.checkSelfPermission(
                        this@RegisterActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
                    //Permission is not granted
                    permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }

                if (permissionsList.isEmpty()) {
                    //不用申请权限了，直接显示拍照页面
                    showTackPhotoView()
                } else {
                    //还需先申请权限
                    ActivityCompat.requestPermissions(this@RegisterActivity,
                        //由List构建Array
                        Array(permissionsList.size) {
                                i-> permissionsList[i]}, ASK_PERMISSIONS)
                }
            }
            view.findViewById<TextView>(R.id.sheetItemSelectPicture).setOnClickListener {
                //从图库中选
            }
            view.findViewById<TextView>(R.id.sheetItemCancel).setOnClickListener {
                //隐藏Sheet
                sheetDialog?.dismiss()
            }
            sheetDialog!!.setContentView(view)
            sheetDialog!!.show()
        }

        //点击了提交按钮，注册之
        activityBinding.content.buttonCommit.setOnClickListener { v1 ->
            //产生文件Part
            val filePart = createFilePart()
            //产生文本Part
            val name = activityBinding.content.editTextName.text.toString()
            val password = activityBinding.content.editTextPassword.text.toString()
            //从Retrofit获取RxJava observable对象
            val observable = chatService!!.requestRegister(filePart!!, name, password)

            //设置数据转换回调
            observable.map{
                if(it.retCode === 0){
                    it.data!! //返回的是一个ContactsPageListAdapter.ContactInfo
                }else{
                    throw RuntimeException(it.errMsg)
                }
            }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                    //Observer的范型参数必须与map的输出类型一致
                .subscribe(object : io.reactivex.Observer<ContactsPageListAdapter.ContactInfo>{
                    override fun onSubscribe(d: Disposable) {
                        this@RegisterActivity.disposable = d
                    }

                    override fun onNext(contactInfo: ContactsPageListAdapter.ContactInfo) {
                        //提示用户注册成功
                        Snackbar.make(v1, "注册成功！", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                        //关闭Activity，返回OK
                        setResult(Activity.RESULT_OK)
                        finish()
                    }

                    override fun onError(e: Throwable) {
                        //在这里捕获各种异常，提示错误信息
                        val errmsg = e.localizedMessage
                        Snackbar.make(v1, "大王祸事了：" + errmsg!!, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                        Log.e("qqserver", e.localizedMessage!!)
                    }

                    override fun onComplete() {
                    }
                })
        }

        activityBinding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    private fun createFilePart(): MultipartBody.Part? {
        if (this.imageUri == null) {
            //必须有个Part才行，所以创建一个吧
            return MultipartBody.Part.createFormData("none", "none")
        }

        var inputStream: InputStream? = null
        var data: ByteArray?
        try {
            inputStream = contentResolver.openInputStream(this.imageUri!!)
            data = ByteArray(inputStream!!.available())
            inputStream.read(data)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            return null
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

        val requestFile = data.toRequestBody("application/otcet-stream".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData("file", "png", requestFile)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            TAKE_PHOTO -> {
                val intent = Intent("com.android.camera.action.CROP") //剪裁
                //告诉剪裁Activity，要申请对Uri的读和写权限，因为编辑后的图像还是存到这个文件中
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                            or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                intent.setDataAndType(this.imageUri, "image/*")
                intent.putExtra("scale", true)
                intent.putExtra("crop", true)
                //设置宽高比例
                intent.putExtra("aspectX", 1)
                intent.putExtra("aspectY", 1)
                //设置裁剪图片宽高
                intent.putExtra("outputX", 480)
                intent.putExtra("outputY", 480)
                //设置图像输出到的文件，跟输入文件是同一个
                intent.putExtra(MediaStore.EXTRA_OUTPUT, this.imageUri)
                startActivityForResult(intent, CROP_PHOTO)
                //隐藏Sheet
                sheetDialog?.dismiss()
            }
            SELECT_PHOTO -> {
                //从图库中选择一个照片，请自行实现

            }
            CROP_PHOTO -> try {
                //图片解析成Bitmap对象
                val bitmap = BitmapFactory.decodeStream(
                    contentResolver.openInputStream(this.imageUri!!)
                )
                //将剪裁后照片显示出来
                activityBinding.content.imageViewAvatar.setImageBitmap(bitmap)
                //隐藏Sheet
                sheetDialog?.dismiss()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            else -> {
                //默认执行逻辑
            }
        }
    }

    override fun onStop() {
        this.disposable?.let {
            if (!it.isDisposed){
                it.dispose() //取消订阅
            }
        }
        super.onStop()
    }

    private fun showTackPhotoView() {
        //获取图像要保存到的文件对象
        val imageOutputFile = generateOutPutFile(Environment.DIRECTORY_DCIM)
        //由文件对象获取Uri对象，它相当于文件的绝对路径
        this.imageUri = FileProvider.getUriForFile(
            this,
            "niuedu.com.qqapp.fileprovider",
            imageOutputFile!!
        )
        //创建Intent，启动拍照Activity
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE) //照相
        intent.putExtra(MediaStore.EXTRA_OUTPUT, this.imageUri) //指定图片输出地址
        //告诉拍照Activity，要申请Uri的读和写权限
        intent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
        startActivityForResult(intent, TAKE_PHOTO) //启动照相
    }

    private fun generateOutPutFile(pathInExternalStorage: String): File? {
        //图片名称 时间命名
        val format = SimpleDateFormat("yyyyMMddHHmmss")
        val date = Date(System.currentTimeMillis())
        val photoFileName = format.format(date) + ".png"
        //创建File对象用于存储拍照的图片,存储至外部存储的公开目录下
        val path = Environment.getExternalStoragePublicDirectory(pathInExternalStorage)
        val outputFile = File(path, photoFileName)
        try {
            if (outputFile.exists()) {
                outputFile.delete()
            }
            outputFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return outputFile
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            ASK_PERMISSIONS -> {
                for (i in 0 until permissions.size) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(
                            this,
                            "权限申请被拒绝，无法完成照片选择。",
                            Toast.LENGTH_SHORT
                        ).show()
                        return
                    }
                }
                //能执行到这里说明全部通过，拍照！
                showTackPhotoView()
                //关掉Sheet
                sheetDialog?.dismiss()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

}
